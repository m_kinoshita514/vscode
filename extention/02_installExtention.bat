@echo off
cd /d %~dp0

echo VisualStudioCodeへ拡張機能を一括インストールします。
timeout 5

for /f %%i in (extentions.txt) do code --install-extension %%i

echo VisualStudioCodeの拡張機能をインストールしました。

pause
exit /b 0
