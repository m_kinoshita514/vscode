@echo off
cd /d %~dp0

echo VisualStudioCodeの拡張機能をリスト出力します。
timeout 5

code --list-extensions > extentions.txt

echo VisualStudioCodeの拡張機能をリスト出力しました。

pause
exit /b 0
